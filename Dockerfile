FROM ubuntu:latest
RUN apt update \
    && DEBIAN_FRONTEND=noninteractive apt install -yq --no-install-recommends \
        graphviz \
        openjdk-8-jre \
    && apt autoremove -y \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && mkdir -p /opt/plantuml/

ADD plantuml.jar /opt/plantuml/plantuml.jar
ADD plantuml.sh /opt/plantuml/
RUN chmod +x /opt/plantuml/plantuml.sh \
    && ln -s /opt/plantuml/plantuml.sh /usr/bin/plantuml \
    && chmod 755 /opt/plantuml/plantuml.jar \
    && chmod 755 /opt/plantuml/plantuml.sh
